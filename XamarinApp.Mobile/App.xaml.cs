﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinApp.Mobile
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class App : Application
    {

        public static IServiceProvider ServiceProvider { get; set; }
        public App()
        {
            this.InitializeComponent();
            this.MainPage = ServiceProvider.GetService<AppShell>();
        }
    }
}