﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XamarinApp.Mobile.ViewModels
{
    public class BaseViewModel
    {
        #region Properties

        public string Hello { get; set; }

        #endregion

        #region Constructors

        public BaseViewModel()
        {

        }

        #endregion
    }
}
