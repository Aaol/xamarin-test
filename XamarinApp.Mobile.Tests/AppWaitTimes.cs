﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.UITest.Utils;

namespace XamarinApp.Mobile.UITests
{
    class AppWaitTimes : IWaitTimes
    {
        private const double TIMEOUT = 0.5;

        public TimeSpan WaitForTimeout => TimeSpan.FromMinutes(TIMEOUT);

        public TimeSpan GestureWaitTimeout => TimeSpan.FromMinutes(TIMEOUT);

        public TimeSpan GestureCompletionTimeout => TimeSpan.FromMinutes(TIMEOUT);
    }
}
