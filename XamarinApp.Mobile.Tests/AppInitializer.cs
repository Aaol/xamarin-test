﻿using System;
using System.IO;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace XamarinApp.Mobile.UITests
{
    public class AppInitializer
    {
        public static IApp StartApp(Platform platform)
        {
            if (platform == Platform.Android)
            {
                return ConfigureApp.Android
                    //.WaitTimes(new AppWaitTimes())
                    .EnableLocalScreenshots()
                    .ApkFile(
                        Path.Combine(new string[]
                        {
                            System.Environment.ExpandEnvironmentVariables("%appdata%"),
                            "../",
                            "Local",
                            "Xamarin",
                            "Mono for Android",
                            "Archives",
                            "2020-03-02",
                            "XamarinApp.Mobile.Droid 3-02-20 1.47 PM.apkarchive",
                            "com.companyname.xamarinapp.mobile.droid.apk"
                        }))
                    .StartApp();
            }

            return ConfigureApp.iOS.StartApp();
        }
    }
}